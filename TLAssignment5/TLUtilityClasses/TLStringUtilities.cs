﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace TLAssignment5.TLUtilityClasses
{
    public static class TLStringUtilities
    {
        public static string FirstCap(string inputText)
        {
            char[] tempArray = inputText.ToCharArray();
            //for first char
            if (tempArray.Length >= 1)
            {
                if (char.IsLower(tempArray[0]))
                {
                    tempArray[0] = char.ToUpper(tempArray[0]);
                }
            }
            // for the rest
            for (int i = 1; i < tempArray.Length; i++)
            {
                // a space at i-1 mean char at i is the beginning
                // of next word
                if (tempArray[i - 1] == ' ')
                {
                    if (char.IsLower(tempArray[i]))
                    {
                        tempArray[i] = char.ToUpper(tempArray[i]);
                    }
                }
            }
            //return a string base on the tempArray
            return new string(tempArray);
        }
        public static string DigitsOnly(string inputText)
        {

            return inputText;
        }

    }
}
